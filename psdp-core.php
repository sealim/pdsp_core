<?php

/*
* Plugin Name:       _PDSP Core Addon
* Plugin URI:        pdsp-core.php
* Description:       This plugin mainly creates custom post types and governs the site's css.
* Version:           1.0.0
* Author:            sealim
* Author URI:        uri
* License:           GPL-2.0+
* License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
* Text Domain:       pdsp
*/

register_activation_hook( __FILE__, 'pdsp_activate_plugin_pdsp_core' );
function pdsp_activate_plugin_pdsp_core() {
    // default_role = scj_participants_singpass
    // create email templates
    // create pages
    require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
    global $wpdb;

}

/*************************
* CUSTOM POST TYPES + METABOXES + CLASSES
*************************/
//cpt
require ( dirname(__FILE__) . '/cpt/pdsp-add-cpt.php' );

// metaboxes
// require ( dirname(__FILE__) . '/metaboxes/scj-metabox-email-template.php' );
require ( dirname(__FILE__) . '/metaboxes/pdsp-metabox-maintenance.php' );
require ( dirname(__FILE__) . '/metaboxes/pdsp-metabox-l3-content.php' );
// require ( dirname(__FILE__) . '/metaboxes/scj-metabox-location-review.php' );
require ( dirname(__FILE__) . '/metaboxes/pdsp-metabox-user-extra-fields.php' );
// require ( dirname(__FILE__) . '/metaboxes/scj-metabox-faqs.php' );
// require ( dirname(__FILE__) . '/metaboxes/scj-metabox-courses-addon.php' );
// require ( dirname(__FILE__) . '/metaboxes/scj-metabox-communities.php' );
// require ( dirname(__FILE__) . '/metaboxes/scj-metabox-resources.php' );
// require ( dirname(__FILE__) . '/metaboxes/scj-metabox-stories.php' );

// classes
// require ( dirname(__FILE__) . '/classes/scj-send-email.php' );
// require ( dirname(__FILE__) . '/classes/scj-activity-log.php' );

/*************************
* SHORTCODES
*************************/
// require ( dirname(__FILE__) . '/shortcodes/scj-page-not-found.php' );
// require ( dirname(__FILE__) . '/shortcodes/scj-reports.php' );
require ( dirname(__FILE__) . '/shortcodes/pdsp-maintenance.php' );
require ( dirname(__FILE__) . '/shortcodes/pdsp-l3-content.php' );
require ( dirname(__FILE__) . '/shortcodes/pdsp-l3-content-preview.php' );

/*************************
* GLOBAL LOADS
*************************/
// require ( dirname(__FILE__) . '/rules/page-reroute.php' );
// require ( dirname(__FILE__) . '/global-sql.php' );
// require ( dirname(__FILE__) . '/global-variables.php' );
// require ( dirname(__FILE__) . '/global-functions.php' );

/*************************
* IMPORTANT NOTES
* - LOADING SEQUENCE IS IMPORTANT, DESIGN-SYSTEM.CSS SHOULD LOAD BEFORE OTHER CSS FILES
*************************/


/*****
* CSS - PRIVATE
*****/
// add_action('admin_enqueue_scripts', 'scj_add_css_icons');
// function scj_add_css_icons() {
//     wp_enqueue_style( 'scjtheme-icons', get_template_directory_uri() ."/assets/icons/css/icons.css", array(), date("h:i:s") );
// }

/*****
* ENQUEUE JS SCRIPT - PRIVATE
*****/
add_action( 'wp_enqueue_scripts', 'pdsp_enqueue_scripts_general' );
function pdsp_enqueue_scripts_general( $hook ) {
    
	// define script url, enqueue script
	$script_url = plugins_url( 'js/pdsp-general.js', __FILE__ );
	wp_enqueue_script( 'pdsp-general', $script_url, array( 'jquery' ), date("h:i:s") , true);
    
	// create nonce, define ajax url, define script
	$nonce = wp_create_nonce( 'pdsp_ajax_general' );
	$script = array( 'nonce' => $nonce );

	// localize script
	wp_localize_script( 'pdsp-general', 'pdsp_general', $script );
}

add_action( 'admin_enqueue_scripts', 'scj_enqueue_scripts_admin_general' );
function scj_enqueue_scripts_admin_general( $hook ) {
    
	// define script url, enqueue script
	$script_url = plugins_url( 'js/pdsp-ajax-admin.js', __FILE__ );
	wp_enqueue_script( 'pdsp-ajax-admin', $script_url, array( 'jquery' ), date("h:i:s") , true);
    
	// create nonce, define ajax url, define script
	$nonce = wp_create_nonce( 'pdsp_ajax_admin' );
	$script = array( 'nonce' => $nonce );

	// localize script
	wp_localize_script( 'pdsp-ajax-admin', 'pdsp_ajax_admin', $script );
}

