<?php

// add custom post type
add_action( 'init', 'pdsp_add_cpt' );
function pdsp_add_cpt() {
	
	$args_maintenance = array(
		'labels'             => array( 'name' => 'PDSP Maintenance' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'edit.php',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'maintenance' ),
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		// 'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
		'supports'           => array( 'title' ),
		'map_meta_cap'	     => true,
		'capability_type'	 => array('maintenance', 'maintenances'),
		'register_meta_box_cb'=>'pdsp_add_metabox_maintenance' // Callback function to show the custom fields
    );
	register_post_type( 'maintenance', $args_maintenance );

	$args_l3content = array(
		'labels'             	=> array( 'name' => 'L3 Content' ),
		'public'             	=> true,
		'publicly_queryable' 	=> true,
		'show_ui'            	=> true,
		'show_in_menu'       	=> 'edit.php',
		'query_var'          	=> true,
		'rewrite'            	=> array( 'slug' => 'l3-content' ),
		'has_archive'        	=> true,
		'hierarchical'       	=> false,
		'menu_position'      	=> null,
		'supports'          	=> array( 'title' ),
		'map_meta_cap'			=> true,
		'capability_type'		=> array('l3_content', 'l3_contents'),
		'register_meta_box_cb' 	=>'pdsp_add_metabox_l3_content' // Callback function to show the custom fields
	);
	register_post_type( 'l3-content', $args_l3content );
}