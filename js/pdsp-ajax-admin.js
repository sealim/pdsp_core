(function($) {
	
	$(document).ready(function() {
        /****************
        * CLICK ON FILTER at admin.php?page=scj-aa
        ****************/
        $(document).on('click', '#template-preview-custom', function(e){
            e.preventDefault();
            var form_data = {
                "pdsp-metabox-l3-content-template-number": $("[name='pdsp-metabox-l3-content-template-number']").val(),

                "pdsp-metabox-l3-content-article-header": $("[name='pdsp-metabox-l3-content-article-header']").val(),
                "pdsp-metabox-l3-content-article-content1": $("[name='pdsp-metabox-l3-content-article-content1']").val(),
                "pdsp-metabox-l3-content-article-content2": $("[name='pdsp-metabox-l3-content-article-content2']").val(),
                "pdsp-metabox-l3-content-card1-image": $("[name='pdsp-metabox-l3-content-card1-image']").val(),
                "pdsp-metabox-l3-content-card1-title": $("[name='pdsp-metabox-l3-content-card1-title']").val(),
                "pdsp-metabox-l3-content-card1-description": $("[name='pdsp-metabox-l3-content-card1-description']").val(),
                "pdsp-metabox-l3-content-card1-link": $("[name='pdsp-metabox-l3-content-card1-link']").val(),

                "pdsp-metabox-l3-content-card2-image": $("[name='pdsp-metabox-l3-content-card2-image']").val(),
                "pdsp-metabox-l3-content-card2-title": $("[name='pdsp-metabox-l3-content-card2-title']").val(),
                "pdsp-metabox-l3-content-card2-description": $("[name='pdsp-metabox-l3-content-card2-description']").val(),
                "pdsp-metabox-l3-content-card2-link": $("[name='pdsp-metabox-l3-content-card2-link']").val(),

                "pdsp-metabox-l3-content-card3-image": $("[name='pdsp-metabox-l3-content-card3-image']").val(),
                "pdsp-metabox-l3-content-card3-title": $("[name='pdsp-metabox-l3-content-card3-title']").val(),
                "pdsp-metabox-l3-content-card3-description": $("[name='pdsp-metabox-l3-content-card3-description']").val(),
                "pdsp-metabox-l3-content-card3-link": $("[name='pdsp-metabox-l3-content-card3-link']").val(),

                "pdsp-metabox-l3-content-card4-image": $("[name='pdsp-metabox-l3-content-card4-image']").val(),
                "pdsp-metabox-l3-content-card4-title": $("[name='pdsp-metabox-l3-content-card4-title']").val(),
                "pdsp-metabox-l3-content-card4-description": $("[name='pdsp-metabox-l3-content-card4-description']").val(),
                "pdsp-metabox-l3-content-card4-link": $("[name='pdsp-metabox-l3-content-card4-link']").val()
            };

            // console.log(form_data);

            let searchParams = new URLSearchParams(window.location.search)
            let param = searchParams.get('post')

            // console.log(form_data); // for debugging purposes
            $.post(ajaxurl, {
                nonce: pdsp_meta_box_nonce.nonce,
                action: 'metabox_l3_page_hook',
                href: window.location.href,
                formdata: form_data,
                step: "filter",
            }, function(result) {
                window.open("http://www.testpdsp.dev.cc/preview-page-template/?post_id="+param,"wp-preview-template");
                console.log("completed");
            });
        });

    });
})( jQuery );