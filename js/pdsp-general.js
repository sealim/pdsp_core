(function($) {
	$(document).ready(function() {
        $(document).on('click', '#pdsp-alert-banner-close', function(e) {
            e.preventDefault();
            $("#pdsp-alert-banner").css("display", "none");
            var d = new Date();
            var alert_id = $("#maintenance_id").text();
            d.setTime(d.getTime() + (4*60*60*1000)); //4hour expiry
            var expires = "expires=" + d.toGMTString();
            document.cookie = "pdsp_maintenance_alert"+alert_id+"=1;" + expires + ";path=/";
        });

    });
})( jQuery );