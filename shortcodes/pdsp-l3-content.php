<?php


// display specific custom field for each post
add_shortcode( 'pdsp-l3-content', 'pdsp_display_l3_content' );
function pdsp_display_l3_content( $attr ) {
	/*
		get_post_meta(
			int $post_id,
			string $key = '',
			bool $single = false
		)
    */

    $posts_id = pdsp_get_l3_posts(); // use get_the_ID() to get current post id
    // $post_data = get_post( $post_id );
    // $post_type = $post_data->post_type;
    $tab_output = "";
    $tab_content ="";


    // $args = array(
    //     'posts_per_page'   => -1,
    //     'category_name'         => 'uncategorized',
    //     'orderby'          => 'name',
    //     'order'            => 'ASC'
    // );
    
    // $posts = get_posts($args);var_dump($posts);

    $args=array(
        'post_parent' => 0,
        'posts_per_page' => -1, 
        'category_name' => 'self-care',
    );
    $q = new WP_Query( $args );

    global $post;
    $counter = 0;
    foreach ( $q->posts as $post ) {
        setup_postdata($post);
        // echo var_dump($post);
        // here your code e.g.
        $post_title = $post->post_title;
        $content = $post->post_content;
        $post_id = $post->ID;
        // echo $content;
        $show_active_tab = $counter == 0 ? "active" : "";
        $show_active_content = $counter == 0 ? "show active" : "";
        $counter++; 
        $tab_output .= '<button class="nav-link '.$show_active_tab.'" id="v-pills-'.$post_id.'-tab" data-bs-toggle="pill" data-bs-target="#v-pills-'.$post_id.'" type="button" role="tab" aria-controls="v-pills-'.$post_id.'" aria-selected="true">'.$post_title.'</button>';
        $tab_content .= '<div class="tab-pane '.$show_active_content.'" id="v-pills-'.$post_id.'" role="tabpanel" aria-labelledby="v-pills-'.$post_id.'-tab">'.$content.'</div>';
    }
    
    $append_output = '<div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="lc-block">
                                    <div class="d-flex align-items-start">
                                        <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="border-top: 2px solid #433663;">
                                            '.$tab_output.'
                                        </div>
                                        <div class="tab-content" id="v-pills-tabContent">
                                            '.$tab_content.'
                                        </div>
                                    </div>
                                </div><!-- /lc-block -->
                            </div><!-- /col -->
                        </div>
                    </div>';

    return $append_output;
}

function pdsp_get_l3_posts(){
    global $wpdb;
    global $charset_collate;
    $results = $wpdb->get_results( 
        $wpdb->prepare("SELECT ID FROM {$wpdb->prefix}posts WHERE post_type=%s AND post_status=%s",
        array('l3-content','publish')), ARRAY_A);
    return $results;
}