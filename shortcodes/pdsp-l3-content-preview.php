<?php


// display specific custom field for each post
add_shortcode( 'pdsp-l3-content-preview', 'pdsp_display_l3_content_preview' );
function pdsp_display_l3_content_preview( $attr ) {
	/*
		get_post_meta(
			int $post_id,
			string $key = '',
			bool $single = false
		)
    */
    
    if (isset($_GET['post_id'])) {
        $post_id = $_GET['post_id'];
    }
    // $post_data = pdsp_get_l3_post($post_id);
    $form_data = $_SESSION['form_data'];
    // var_dump( $form_data );
    

    $tab_output = "";
    $tab_content ="";

    $template_number = get_post_meta( $post_id, '_pdsp_metakey_l3_content_template_number', true );

    if ($form_data['pdsp-metabox-l3-content-template-number'] == 1) {

        if($form_data['pdsp-metabox-l3-content-card1-link']==null || $form_data['pdsp-metabox-l3-content-card1-link']==''){
            $card1_link = '" style="display: none;"';
        }
        if($form_data['pdsp-metabox-l3-content-card2-link']==null || $form_data['pdsp-metabox-l3-content-card2-link']==''){
            $card2_link = '" style="display: none;"';
        }
        if($form_data['pdsp-metabox-l3-content-card3-link']==null || $form_data['pdsp-metabox-l3-content-card3-link']==''){
            $card3_link = '" style="display: none;"';
        }
        if($form_data['pdsp-metabox-l3-content-card4-link']==null || $form_data['pdsp-metabox-l3-content-card4-link']==''){
            $card4_link = '" style="display: none;"';
        }

        $tab_content .= '<div class="tab-pane active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
            <div class="lc-block row" id="l2-template1-title">
                <div editable="rich">
                    <h1>'.$form_data['pdsp-metabox-l3-content-article-header'].'</h1>
                </div>
            </div>
            <div class="lc-block row mb-3" id="l2-template-text">
                <div editable="rich">
                    <p>'.$form_data['pdsp-metabox-l3-content-article-content1'].'</p>
                    <p></p>
                    <p></p>
                    <p>'.$form_data['pdsp-metabox-l3-content-article-content2'].'</p>
                </div>
            </div>
            <div class="lc-block row pb-2" style="">
                <div class="col-6 l2-template1-cards">
                    <div class="card l3-template1-card">
                        <img class="card-img-top" src="'.$form_data['pdsp-metabox-l3-content-card1-image'].'">
                        <div class="card-body l3-template1-card-body">
                            <div class="lc-block">
                                <div editable="rich">
                                    <p class="template1-card-title">'.$form_data['pdsp-metabox-l3-content-card1-title'].'</p>
                                    <!-- <h4 class="card-subtitle" editable="inline">Card subtitle</h4> -->
                                    <p class="template1-card-text">'.$form_data['pdsp-metabox-l3-content-card1-description'].'</p>
                                    <a class="l2-view-more-btn" href="'.$card1_link.'" role="button">
                                        View More
                                        <img src="https://img.icons8.com/ios/50/000000/circled-chevron-right.png" class="ms-2" data-pagespeed-url-hash="3820130607" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 mt-6 l2-template1-cards">
                    <div class="card l3-template1-card">
                        <img class="card-img-top" src="'.$form_data['pdsp-metabox-l3-content-card2-image'].'">
                        <div class="card-body l3-template1-card-body">
                            <div class="lc-block">
                                <div editable="rich">
                                    <p class="template1-card-title">'.$form_data['pdsp-metabox-l3-content-card2-title'].'</p>
                                    <!-- <h4 class="card-subtitle" editable="inline">Card subtitle</h4> -->
                                    <p class="template1-card-text">'.$form_data['pdsp-metabox-l3-content-card2-description'].'</p>
                                    <a class="l2-view-more-btn" href="'.$card2_link.'" role="button">
                                        View More
                                        <img src="https://img.icons8.com/ios/50/000000/circled-chevron-right.png" class="ms-2" data-pagespeed-url-hash="3820130607" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lc-block row pb-2" style="">
                <div class="col-6 l2-template1-cards">
                    <div class="card l3-template1-card">
                        <img class="card-img-top" src="'.$form_data['pdsp-metabox-l3-content-card3-image'].'">
                        <div class="card-body l3-template1-card-body">
                            <div class="lc-block">
                                <div editable="rich">
                                    <p class="template1-card-title">'.$form_data['pdsp-metabox-l3-content-card3-title'].'</p>
                                    <!-- <h4 class="card-subtitle" editable="inline">Card subtitle</h4> -->
                                    <p class="template1-card-text">'.$form_data['pdsp-metabox-l3-content-card3-description'].'</p>
                                    <a class="l2-view-more-btn" href="'.$card3_link.'" role="button">
                                        View More
                                        <img src="https://img.icons8.com/ios/50/000000/circled-chevron-right.png" class="ms-2" data-pagespeed-url-hash="3820130607" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 mt-6 l2-template1-cards">
                    <div class="card l3-template1-card">
                        <img class="card-img-top" src="'.$form_data['pdsp-metabox-l3-content-card4-image'].'">
                        <div class="card-body l3-template1-card-body">
                            <div class="lc-block">
                                <div editable="rich">
                                    <p class="template1-card-title">'.$form_data['pdsp-metabox-l3-content-card4-title'].'</p>
                                    <!-- <h4 class="card-subtitle" editable="inline">Card subtitle</h4> -->
                                    <p class="template1-card-text">'.$form_data['pdsp-metabox-l3-content-card4-description'].'</p>
                                    <a class="l2-view-more-btn" href="'.$card4_link.'" role="button">
                                        View More
                                        <img src="https://img.icons8.com/ios/50/000000/circled-chevron-right.png" class="ms-2" data-pagespeed-url-hash="3820130607" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
    } else {
        $tab_content = "Template ".$form_data['pdsp-metabox-l3-content-template-number'];
    }


// ******* START OF BODY ********//
    $post_title = get_post( $post_id )->post_title;

    $tab_output .= '<button class="nav-link active" id="v-pills-1-tab" data-bs-toggle="pill" data-bs-target="#v-pills-1" type="button" role="tab" aria-controls="v-pills-1" aria-selected="true">'.$post_title.'</button>';
            
        

    $append_output = '<div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="lc-block">
                                    <div class="d-flex align-items-start">
                                        <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="border-top: 2px solid #433663;">
                                            '.$tab_output.'
                                        </div>
                                        <div class="tab-content" id="v-pills-tabContent">
                                            '.$tab_content.'
                                        </div>
                                    </div>
                                </div><!-- /lc-block -->
                            </div><!-- /col -->
                        </div>
                    </div>';

    return $append_output;
}

function pdsp_get_l3_post($post_id){
    global $wpdb;
    global $charset_collate;
    $results = $wpdb->get_results( 
        $wpdb->prepare("SELECT ID FROM {$wpdb->prefix}posts WHERE post_type=%s AND post_status=%s AND id=%d",
        array('l3-content','publish'), $post_id), ARRAY_A);
    return $results;
}