<?php


// display specific custom field for each post
add_shortcode( 'pdsp-maintenance', 'pdsp_display_cf_maintenance' );
function pdsp_display_cf_maintenance( $attr ) {
	/*
		get_post_meta(
			int $post_id,
			string $key = '',
			bool $single = false
		)
    */

    $post_id = pdsp_get_maintenance_post_latest()[0]->ID; // use get_the_ID() to get current post id
    $post_data = get_post( $post_id );
    $post_type = $post_data->post_type;


    if ($post_type === 'maintenance'){
        $msg_prelogin = get_post_meta( $post_id, '_pdsp_metakey_maintenance_msg_prelogin', true );
        $msg_postlogin = get_post_meta( $post_id, '_pdsp_metakey_maintenance_msg_postlogin', true );
        $start_date = get_post_meta( $post_id, '_pdsp_metakey_maintenance_start_date', true );
        $start_time = get_post_meta( $post_id, '_pdsp_metakey_maintenance_start_time', true );
        $end_date = get_post_meta( $post_id, '_pdsp_metakey_maintenance_end_date', true );
        $end_time = get_post_meta( $post_id, '_pdsp_metakey_maintenance_end_time', true );
        $x_day = get_post_meta( $post_id, '_pdsp_metakey_maintenance_x_day', true );
        $start = date_create($start_date . ' ' . $start_time);
        $end = date_create($end_date . ' ' . $end_time);
        $today = date_create(date("d F Y g:i A", strtotime('+8 hours'))); //SGT+8 adjusted for server timezone
        $show_day = (clone $start)->modify('-'.intval($x_day).' day');
    
        $append_output = <<<SCRIPT
            <div class="banner" id="pdsp-alert-banner">
                <div class="banner__content pdsp-row d-flex px-4 align-items-center">
                        <span id="maintenance_id" style="display: none">$post_id</span>
                        <span class="icon-circle-warning banner-icons"></span>
            SCRIPT;
            $append_output .= ' <svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" fill="currentColor" viewBox="0 0 16 16" lc-helper="svg-icon">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                                    <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"></path>
                                </svg>';
            if ( !is_user_logged_in() ) {
                $append_output .= '<span class="pdsp-row ms-2 align-items-center banner__text">  ' . str_replace('[end_datetime]', date_format($end,"d M Y g:i A"), str_replace('[start_datetime]',date_format($start,"d M Y g:i A"),$msg_prelogin)) . "</span>";
            } else {
                $append_output .= '<span class="pdsp-row ms-2 align-items-center banner__text">  '. str_replace('[end_datetime]', date_format($end,"d M Y g:i A"), str_replace('[start_datetime]',date_format($start,"d M Y g:i A"),$msg_postlogin)) . "</span>";
            }
            $append_output .= '<button type="button" id="pdsp-alert-banner-close" class="btn-close ms-auto" aria-label="Close"></button>';
            $append_output .= '</div></div>';

            //ADD POSTID to alert cookie checker
            $maintenance_cookie = "pdsp_maintenance_alert".$post_id;

            $alert_shown = json_decode(stripslashes($_COOKIE[ $maintenance_cookie ]), true);
            if (isset($alert_shown)) {
                $append_output = ($alert_shown == "1" ) ? "" : $append_output;
            }

            if( $today >= $show_day && $today <= $end){
                return $append_output;
            } else {
                return;
            }

    } else {
        return;
    }
}

function pdsp_get_maintenance_post_latest () {
    global $wpdb;
    global $charset_collate;
    $results = $wpdb->get_results( 
        $wpdb->prepare("SELECT ID FROM {$wpdb->prefix}posts WHERE post_type=%s AND post_status=%s ORDER BY post_modified DESC LIMIT 1",
        array('maintenance','publish')) 
    );
    return $results;
}