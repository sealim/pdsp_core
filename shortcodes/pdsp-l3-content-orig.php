<?php


// display specific custom field for each post
add_shortcode( 'pdsp-l3-content', 'pdsp_display_l3_content' );
function pdsp_display_l3_content( $attr ) {
	/*
		get_post_meta(
			int $post_id,
			string $key = '',
			bool $single = false
		)
    */

    $posts_id = pdsp_get_l3_posts(); // use get_the_ID() to get current post id
    // $post_data = get_post( $post_id );
    // $post_type = $post_data->post_type;
    $tab_output = "";
    $tab_content ="";

    foreach ($posts_id as $k=>$v) {
        // $this->aa_data['action_meta'][$k] = $v;
        foreach ($v as $key=>$post_id) {
            $article_header = get_post_meta( $post_id, '_pdsp_metakey_l3_content_article_header', true );
            $article_content1 = get_post_meta( $post_id, '_pdsp_metakey_l3_content_article_content1', true );
            $article_content2 = get_post_meta( $post_id, '_pdsp_metakey_l3_content_article_content2', true );
    
            $card1_image = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card1_image', true );
            $card1_title = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card1_title', true );
            $card1_description = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card1_description', true );
            $card1_link = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card1_link', true );

            $card2_image = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card2_image', true );
            $card2_title = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card2_title', true );
            $card2_description = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card2_description', true );
            $card2_link = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card2_link', true );

            $card3_image = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card3_image', true );
            $card3_title = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card3_title', true );
            $card3_description = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card3_description', true );
            $card3_link = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card3_link', true );

            $card4_image = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card4_image', true );
            $card4_title = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card4_title', true );
            $card4_description = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card4_description', true );
            $card4_link = get_post_meta( $post_id, '_pdsp_metakey_l3_content_card4_link', true );

            if($card1_link==null || $card1_link==''){
                $card1_link = '" style="display: none;"';
            }
            if($card2_link==null || $card2_link==''){
                $card2_link = '" style="display: none;"';
            }
            if($card3_link==null || $card3_link==''){
                $card3_link = '" style="display: none;"';
            }
            if($card4_link==null || $card4_link==''){
                $card4_link = '" style="display: none;"';
            }


            $post_title = get_post( $post_id )->post_title;
            $var = $k+1;

            $show_active_tab = $var == 1 ? "active" : "";
            $show_active_content = $var == 1 ? "show active" : "";

            $tab_output .= '<button class="nav-link '.$show_active_tab.'" id="v-pills-'.$var.'-tab" data-bs-toggle="pill" data-bs-target="#v-pills-'.$var.'" type="button" role="tab" aria-controls="v-pills-'.$var.'" aria-selected="true">'.$post_title.'</button>';
            
            $tab_content .= '<div class="tab-pane '.$show_active_content.'" id="v-pills-'.$var.'" role="tabpanel" aria-labelledby="v-pills-'.$var.'-tab">
            <div class="lc-block row" id="l2-template1-title">
                <div editable="rich">
                    <h1>'.$article_header.'</h1>
                </div>
            </div>
            <div class="lc-block row mb-3" id="l2-template-text">
                <div editable="rich">
                    <p>'.$article_content1.'</p>
                    <p></p>
                    <p></p>
                    <p>'.$article_content2.'</p>
                </div>
            </div>
            <div class="lc-block row pb-2" style="">
                <div class="col-6 l2-template1-cards">
                    <div class="card l3-template1-card">
                        <img class="card-img-top" src="'.$card1_image.'">
                        <div class="card-body l3-template1-card-body">
                            <div class="lc-block">
                                <div editable="rich">
                                    <p class="template1-card-title">'.$card1_title.'</p>
                                    <!-- <h4 class="card-subtitle" editable="inline">Card subtitle</h4> -->
                                    <p class="template1-card-text">'.$card1_description.'</p>
                                    <a class="l2-view-more-btn" href="'.$card1_link.'" role="button">
                                        View More
                                        <img src="https://img.icons8.com/ios/50/000000/circled-chevron-right.png" class="ms-2" data-pagespeed-url-hash="3820130607" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 mt-6 l2-template1-cards">
                    <div class="card l3-template1-card">
                        <img class="card-img-top" src="'.$card2_image.'">
                        <div class="card-body l3-template1-card-body">
                            <div class="lc-block">
                                <div editable="rich">
                                    <p class="template1-card-title">'.$card2_title.'</p>
                                    <!-- <h4 class="card-subtitle" editable="inline">Card subtitle</h4> -->
                                    <p class="template1-card-text">'.$card2_description.'</p>
                                    <a class="l2-view-more-btn" href="'.$card2_link.'" role="button">
                                        View More
                                        <img src="https://img.icons8.com/ios/50/000000/circled-chevron-right.png" class="ms-2" data-pagespeed-url-hash="3820130607" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lc-block row pb-2" style="">
                <div class="col-6 l2-template1-cards">
                    <div class="card l3-template1-card">
                        <img class="card-img-top" src="'.$card3_image.'">
                        <div class="card-body l3-template1-card-body">
                            <div class="lc-block">
                                <div editable="rich">
                                    <p class="template1-card-title">'.$card3_title.'</p>
                                    <!-- <h4 class="card-subtitle" editable="inline">Card subtitle</h4> -->
                                    <p class="template1-card-text">'.$card3_description.'</p>
                                    <a class="l2-view-more-btn" href="'.$card3_link.'" role="button">
                                        View More
                                        <img src="https://img.icons8.com/ios/50/000000/circled-chevron-right.png" class="ms-2" data-pagespeed-url-hash="3820130607" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 mt-6 l2-template1-cards">
                    <div class="card l3-template1-card">
                        <img class="card-img-top" src="'.$card4_image.'">
                        <div class="card-body l3-template1-card-body">
                            <div class="lc-block">
                                <div editable="rich">
                                    <p class="template1-card-title">'.$card4_title.'</p>
                                    <!-- <h4 class="card-subtitle" editable="inline">Card subtitle</h4> -->
                                    <p class="template1-card-text">'.$card4_description.'</p>
                                    <a class="l2-view-more-btn" href="'.$card4_link.'" role="button">
                                        View More
                                        <img src="https://img.icons8.com/ios/50/000000/circled-chevron-right.png" class="ms-2" data-pagespeed-url-hash="3820130607" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        
        
        }
    }

    $append_output = '<div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="lc-block">
                                    <div class="d-flex align-items-start">
                                        <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="border-top: 2px solid #433663;">
                                            '.$tab_output.'
                                        </div>
                                        <div class="tab-content" id="v-pills-tabContent">
                                            '.$tab_content.'
                                        </div>
                                    </div>
                                </div><!-- /lc-block -->
                            </div><!-- /col -->
                        </div>
                    </div>';

    return $append_output;
}

function pdsp_get_l3_posts(){
    global $wpdb;
    global $charset_collate;
    $results = $wpdb->get_results( 
        $wpdb->prepare("SELECT ID FROM {$wpdb->prefix}posts WHERE post_type=%s AND post_status=%s",
        array('l3-content','publish')), ARRAY_A);
    return $results;
}