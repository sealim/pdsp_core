<?php

// register meta box, actual adding custom fields to the post
// add_action( 'add_meta_boxes', 'pdsp_add_metabox_maintenance' );
function pdsp_add_metabox_maintenance() {
	$post_types = array( 'maintenance' ); // put in the post types to be included
	foreach ( $post_types as $post_type ) {
		add_meta_box(
			'pdsp_meta_box', // Unique ID of meta box
			'Scheduled Maintenance Information', // Title of meta box
			'pdsp_display_metabox_maintenance', // Callback function to show the custom fields
			$post_type // Post type
		);
	}
}

// display meta box, preparing the meta box
function pdsp_display_metabox_maintenance( $post ) {
    
    $html_output = '';

	$msg_prelogin = get_post_meta( $post->ID, '_pdsp_metakey_maintenance_msg_prelogin', true );
    $msg_postlogin = get_post_meta( $post->ID, '_pdsp_metakey_maintenance_msg_postlogin', true );

	$value_start_date = get_post_meta( $post->ID, '_pdsp_metakey_maintenance_start_date', true );
	$value_start_time = get_post_meta( $post->ID, '_pdsp_metakey_maintenance_start_time', true );
	$value_end_date = get_post_meta( $post->ID, '_pdsp_metakey_maintenance_end_date', true );
	$value_end_time = get_post_meta( $post->ID, '_pdsp_metakey_maintenance_end_time', true );
	$value_x_day = get_post_meta( $post->ID, '_pdsp_metakey_maintenance_x_day', true );

    wp_nonce_field( basename( __FILE__ ), 'pdsp_meta_box_nonce' );

    $html_output .= "
        <p><strong>Maintenance Messages</strong></p>
        <p>Variables available:</p>
            <ul style='list-style-type:circle;padding-left: 40px;'>
                <li>[start_datetime]</li>
                <li>[end_datetime]</li>
            </ul>
        <table style='width:100%'>
            <tr>
                <td>Pre-login Message</td>
                <td><textarea name='pdsp-metabox-maintenance-msg-prelogin' style='width:100%'>".esc_textarea($msg_prelogin)."</textarea></td>
            </tr>
            <tr>
                <td>Post-login Message</td>
                <td><textarea name='pdsp-metabox-maintenance-msg-postlogin' style='width:100%'>".esc_textarea($msg_postlogin)."</textarea></td>
            </tr>
        </table>
        ";

    $html_output .= "
        <p><strong>Maintenance Schedule</strong></p>
        <table style='width:100%'>
            <tr>
                <td><label for='pdsp-metabox-maintenance-start-date'>Start Date</label></td>
                <td><input id='pdsp-metabox-maintenance-start-date' name='pdsp-metabox-maintenance-start-date' type='date' value=".esc_attr($value_start_date)."></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-maintenance-start-time'>Start Time</label></td>
                <td><input id='pdsp-metabox-maintenance-start-time' name='pdsp-metabox-maintenance-start-time' type='time' value=".esc_attr($value_start_time)."></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-maintenance-end-date'>End Date</label></td>
                <td><input id='pdsp-metabox-maintenance-end-date' name='pdsp-metabox-maintenance-end-date' type='date' value=".esc_attr($value_end_date)."></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-maintenance-end-time'>End Time</label></td>
                <td><input id='pdsp-metabox-maintenance-end-time' name='pdsp-metabox-maintenance-end-time' type='time' value=".esc_attr($value_end_time)."></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-maintenance-x-day'>Switch on how many days before?</label></td>
                <td><input id='pdsp-metabox-maintenance-x-day' name='pdsp-metabox-maintenance-x-day' type='number' min='1' value=".esc_attr($value_x_day)."></td>
            </tr>
        </table>
        ";

    echo $html_output;
}

// save meta box
add_action( 'save_post', 'pdsp_save_metabox_maintenance' );
function pdsp_save_metabox_maintenance( $post_id ) {

	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = false;
	if ( isset( $_POST[ 'pdsp_meta_box_nonce' ] ) ) {
		if ( wp_verify_nonce( $_POST[ 'pdsp_meta_box_nonce' ], basename( __FILE__ ) ) ) {
			$is_valid_nonce = true;
		}
	}
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) return;
    
    //posting of data into database of wp_postmeta
	if ( array_key_exists( 'pdsp-metabox-maintenance-msg-prelogin', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_maintenance_msg_prelogin',sanitize_text_field( $_POST[ 'pdsp-metabox-maintenance-msg-prelogin' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-maintenance-msg-postlogin', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_maintenance_msg_postlogin',sanitize_text_field( $_POST[ 'pdsp-metabox-maintenance-msg-postlogin' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-maintenance-start-date', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_maintenance_start_date',sanitize_text_field( $_POST[ 'pdsp-metabox-maintenance-start-date' ] ));
	}
    if ( array_key_exists( 'pdsp-metabox-maintenance-start-time', $_POST ) ) {
        update_post_meta($post_id,'_pdsp_metakey_maintenance_start_time',sanitize_text_field( $_POST[ 'pdsp-metabox-maintenance-start-time' ] ));
    }
	if ( array_key_exists( 'pdsp-metabox-maintenance-end-date', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_maintenance_end_date',sanitize_text_field( $_POST[ 'pdsp-metabox-maintenance-end-date' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-maintenance-end-time', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_maintenance_end_time',sanitize_text_field( $_POST[ 'pdsp-metabox-maintenance-end-time' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-maintenance-x-day', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_maintenance_x_day',sanitize_text_field( $_POST[ 'pdsp-metabox-maintenance-x-day' ] ));
	}
}