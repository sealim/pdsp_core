<?php

// register meta box, actual adding custom fields to the post
// add_action( 'add_meta_boxes', 'pdsp_add_metabox_l3_content' );
function pdsp_add_metabox_l3_content() {
	$post_types = array( 'l3-content' ); // put in the post types to be included
	foreach ( $post_types as $post_type ) {
		add_meta_box(
			'pdsp_meta_box', // Unique ID of meta box
			'Content Details', // Title of meta box
			'pdsp_display_metabox_l3_content', // Callback function to show the custom fields
			$post_type // Post type
		);
	}
}

// display meta box, preparing the meta box
function pdsp_display_metabox_l3_content( $post ) {
    
    $template_number = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_template_number', true );
    
    $template_options = "";
    for($i=1; $i<=4; $i++){
        $selected = $template_number == $i ? "selected" : "";
        $template_options .= "<option value='".$i."' ".$selected.">".$i."</option>";
    }

    $html_output = "<h3><strong>Template Number</strong></h3>
        <table style='width:100%'>
            <tr>
                <td>
                    <label for='pdsp-metabox-l3-content-template-number'>Choose template: &emsp;</label>
                    <select id='pdsp-metabox-l3-content-template-number' name='pdsp-metabox-l3-content-template-number'>
                        ".$template_options."
                    </select>
                </td>
                <td>
                <a class='preview button' href='/preview-page-template?post_id=".$post->ID."' target='wp-preview-template' id='template-preview-custom'>Preview Changes</a>
                </td>
            </tr>
        </table>
        ";

    


	$article_header = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_article_header', true );
    $article_content1 = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_article_content1', true );
    $article_content2 = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_article_content2', true );

    $card1_image = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card1_image', true );
	$card1_title = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card1_title', true );
	$card1_description = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card1_description', true );
	$card1_link = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card1_link', true );

    $card2_image = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card2_image', true );
	$card2_title = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card2_title', true );
	$card2_description = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card2_description', true );
	$card2_link = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card2_link', true );

    $card3_image = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card3_image', true );
	$card3_title = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card3_title', true );
	$card3_description = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card3_description', true );
	$card3_link = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card3_link', true );

    $card4_image = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card4_image', true );
	$card4_title = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card4_title', true );
	$card4_description = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card4_description', true );
	$card4_link = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_card4_link', true );
	
	// $value_x_day = get_post_meta( $post->ID, '_pdsp_metakey_l3_content_x_day', true );

    wp_nonce_field( basename( __FILE__ ), 'pdsp_meta_box_nonce' );

    $html_output .= "
        <h3><strong>Content Details</strong></h3>
        <table style='width:100%'>
            <tr>
                <td>Article header</td>
                <td><textarea name='pdsp-metabox-l3-content-article-header' style='width:100%'>".esc_textarea($article_header)."</textarea></td>
            </tr>
            <tr>
                <td>Article content 1:</td>
                <td><textarea name='pdsp-metabox-l3-content-article-content1' style='width:100%'>".esc_textarea($article_content1)."</textarea></td>
            </tr>
            <tr>
                <td>Article content 2:</td>
                <td><textarea name='pdsp-metabox-l3-content-article-content2' style='width:100%'>".esc_textarea($article_content2)."</textarea></td>
            </tr>
        </table>
        ";

    $html_output .= "
        <br>
        <h3><strong>Card 1 Content:</strong></h3>
        <table style='width:100%'>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card1-image'>Card Image</label></td>
                <td><input id='pdsp-metabox-l3-content-card1-image' name='pdsp-metabox-l3-content-card1-image' type='text' style='width:100%' value=".esc_textarea($card1_image)."></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card1-title'>Card Title</label></td>
                <td><input id='pdsp-metabox-l3-content-card1-title' name='pdsp-metabox-l3-content-card1-title' type='text' style='width:50%' value='".esc_html($card1_title)."'></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card1-description'>Card Description</label></td>
                <td><textarea id='pdsp-metabox-l3-content-card1-description' name='pdsp-metabox-l3-content-card1-description' style='width:100%' type='text'>".esc_attr($card1_description)."</textarea></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card1-link'>Card link to</label></td>
                <td><input id='pdsp-metabox-l3-content-card1-link' name='pdsp-metabox-l3-content-card1-link' type='text' style='width:50%' value=".esc_attr($card1_link)."></td>
            </tr>
        </table>

        <h3><strong>Card 2 Content:</strong></h3>
        <table style='width:100%'>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card2-image'>Card Image</label></td>
                <td><input id='pdsp-metabox-l3-content-card2-image' name='pdsp-metabox-l3-content-card2-image' type='text' style='width:100%' value=".esc_textarea($card2_image)."></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card2-title'>Card Title</label></td>
                <td><input id='pdsp-metabox-l3-content-card2-title' name='pdsp-metabox-l3-content-card2-title' type='text' style='width:50%' value='".esc_attr($card2_title)."'></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card2-description'>Card Description</label></td>
                <td><textarea id='pdsp-metabox-l3-content-card2-description' name='pdsp-metabox-l3-content-card2-description' style='width:100%' type='text'>".esc_attr($card2_description)."</textarea></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card2-link'>Card link to</label></td>
                <td><input id='pdsp-metabox-l3-content-card2-link' name='pdsp-metabox-l3-content-card2-link' type='text' style='width:50%' value=".esc_attr($card2_link)."></td>
            </tr>
        </table>

        <h3><strong>Card 3 Content:</strong></h3>
        <table style='width:100%'>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card3-image'>Card Image</label></td>
                <td><input id='pdsp-metabox-l3-content-card3-image' name='pdsp-metabox-l3-content-card3-image' type='text' style='width:100%' value=".esc_textarea($card3_image)."></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card3-title'>Card Title</label></td>
                <td><input id='pdsp-metabox-l3-content-card3-title' name='pdsp-metabox-l3-content-card3-title' type='text' style='width:50%' value='".esc_attr($card3_title)."'></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card3-description'>Card Description</label></td>
                <td><textarea id='pdsp-metabox-l3-content-card3-description' name='pdsp-metabox-l3-content-card3-description' style='width:100%' type='text'>".esc_attr($card3_description)."</textarea></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card3-link'>Card link to</label></td>
                <td><input id='pdsp-metabox-l3-content-card3-link' name='pdsp-metabox-l3-content-card3-link' type='text' style='width:50%' value=".esc_attr($card3_link)."></td>
            </tr>
        </table>

        <h3><strong>Card 4 Content:</strong></h3>
        <table style='width:100%'>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card4-image'>Card Image</label></td>
                <td><input id='pdsp-metabox-l3-content-card4-image' name='pdsp-metabox-l3-content-card4-image' type='text' style='width:100%' value=".esc_textarea($card4_image)."></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card4-title'>Card Title</label></td>
                <td><input id='pdsp-metabox-l3-content-card4-title' name='pdsp-metabox-l3-content-card4-title' type='text' style='width:50%' value='".esc_attr($card4_title)."'></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card4-description'>Card Description</label></td>
                <td><textarea id='pdsp-metabox-l3-content-card4-description' name='pdsp-metabox-l3-content-card4-description' style='width:100%' type='text'>".esc_attr($card4_description)."</textarea></td>
            </tr>
            <tr>
                <td><label for='pdsp-metabox-l3-content-card4-link'>Card link to</label></td>
                <td><input id='pdsp-metabox-l3-content-card4-link' name='pdsp-metabox-l3-content-card4-link' type='text' style='width:50%' value=".esc_attr($card4_link)."></td>
            </tr>
        </table>
        ";
    echo $html_output;
}

add_action( 'wp_ajax_metabox_l3_page_hook', 'pdsp_ajax_metabox_l3_page_handler' ); // ajax hook for logged-in users: wp_ajax_{action}
function pdsp_ajax_metabox_l3_page_handler() {

    $step = isset( $_POST['step'] ) ? $_POST['step'] : '';

    if ( isset($_POST['formdata']) ) {
        $_SESSION['form_data'] = $_POST['formdata'];
    } 
    

}



// save meta box
add_action( 'save_post', 'pdsp_save_metabox_l3_content' );
function pdsp_save_metabox_l3_content( $post_id ) {

	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = false;
	if ( isset( $_POST[ 'pdsp_meta_box_nonce' ] ) ) {
		if ( wp_verify_nonce( $_POST[ 'pdsp_meta_box_nonce' ], basename( __FILE__ ) ) ) {
			$is_valid_nonce = true;
		}
	}
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) return;
    
    //posting of data into database of wp_postmeta
    if ( array_key_exists( 'pdsp-metabox-l3-content-template-number', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_template_number',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-template-number' ] ));
	}

	if ( array_key_exists( 'pdsp-metabox-l3-content-article-header', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_article_header',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-article-header' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-l3-content-article-content1', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_article_content1',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-article-content1' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-l3-content-article-content2', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_article_content2',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-article-content2' ] ));
	}

    if ( array_key_exists( 'pdsp-metabox-l3-content-card1-image', $_POST ) ) {
        update_post_meta($post_id,'_pdsp_metakey_l3_content_card1_image',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card1-image' ] ));
    }
	if ( array_key_exists( 'pdsp-metabox-l3-content-card1-title', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card1_title',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card1-title' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-l3-content-card1-description', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card1_description',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card1-description' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-l3-content-card1-link', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card1_link',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card1-link' ] ));
	}

    if ( array_key_exists( 'pdsp-metabox-l3-content-card2-image', $_POST ) ) {
        update_post_meta($post_id,'_pdsp_metakey_l3_content_card2_image',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card2-image' ] ));
    }
	if ( array_key_exists( 'pdsp-metabox-l3-content-card2-title', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card2_title',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card2-title' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-l3-content-card2-description', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card2_description',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card2-description' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-l3-content-card2-link', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card2_link',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card2-link' ] ));
	}

    if ( array_key_exists( 'pdsp-metabox-l3-content-card3-image', $_POST ) ) {
        update_post_meta($post_id,'_pdsp_metakey_l3_content_card3_image',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card3-image' ] ));
    }
	if ( array_key_exists( 'pdsp-metabox-l3-content-card3-title', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card3_title',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card3-title' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-l3-content-card3-description', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card3_description',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card3-description' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-l3-content-card3-link', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card3_link',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card3-link' ] ));
	}

    if ( array_key_exists( 'pdsp-metabox-l3-content-card4-image', $_POST ) ) {
        update_post_meta($post_id,'_pdsp_metakey_l3_content_card4_image',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card4-image' ] ));
    }
	if ( array_key_exists( 'pdsp-metabox-l3-content-card4-title', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card4_title',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card4-title' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-l3-content-card4-description', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card4_description',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card4-description' ] ));
	}
	if ( array_key_exists( 'pdsp-metabox-l3-content-card4-link', $_POST ) ) {
		update_post_meta($post_id,'_pdsp_metakey_l3_content_card4_link',sanitize_text_field( $_POST[ 'pdsp-metabox-l3-content-card4-link' ] ));
	}
}